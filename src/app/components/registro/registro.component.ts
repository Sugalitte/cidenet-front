import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Persona} from '../../domain/Persona';
import {UsuariosServices} from '../../services/usuariosServices';
import {dominios, paises} from '../../../environments/environment.variables';
import * as moment from 'moment';

@Component({
    selector: 'app-registro',
    templateUrl: './registro.component.html',
    styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
    formUser: FormGroup;
    estado: string;
    requestUser = new Persona();
    correo: string;
    fechaMaxima = moment(moment().format('YYYY-MM-DD'));
    sysdate: Date;


    constructor(private router: Router, private services: UsuariosServices) {
        this.fechaRegistroIn();
        this.estado = 'ACTIVO';
    }

    ngOnInit() {
        this.initForm();

    }

    nav() {
        this.router.navigate(['welcome']);
    }

    initForm() {
        this.formUser = new FormGroup({
                primerApellido: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
                segundoApellido: new FormControl(null, [Validators.maxLength(20)]),
                primerNombre: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
                segundoNombre: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
                pais: new FormControl(null, [Validators.required]),
                tipoId: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
                numId: new FormControl(null, [Validators.required]),
                fechaIngreso: new FormControl(null, [Validators.required]),
                area: new FormControl(null, [Validators.required]),
                estado: new FormControl(null),
                fechaRegistro: new FormControl(null),
            }
        );
    }

    fechaRegistroIn() {
        this.sysdate = new Date();
        //this.sysdate = moment().format('YYYY-MM-DD');
    }

    crearCorreo(requestUser: Persona) {
        if (requestUser.pais == paises.Colombia) {
            this.correo = requestUser.primerNombre.concat('.').concat(requestUser.primerApellido).concat(dominios.Colombia).toLowerCase();
        } else if (requestUser.pais == paises.USA) {
            this.correo = requestUser.primerNombre.concat('.').concat(requestUser.primerApellido).concat(dominios.USA).toLowerCase();
        }
    }

    registrarUsuario() {
        //Creamos el rq
        if (this.formUser.invalid) {
            alert('Faltan campos Obligatorios.');
            return;
        }
        debugger
        this.requestUser.primerApellido = this.formUser.get('primerApellido').value.toUpperCase();
        this.requestUser.segundoApellido = this.formUser.get('segundoApellido').value.toUpperCase();
        this.requestUser.primerNombre = this.formUser.get('primerNombre').value.toUpperCase();
        this.requestUser.segundoNombre = this.formUser.get('segundoNombre').value.toUpperCase();
        this.requestUser.pais = this.formUser.get('pais').value.toUpperCase();
        this.requestUser.tipoIdentificacion = this.formUser.get('tipoId').value.toUpperCase();
        this.requestUser.numeroIdentificacion = this.formUser.get('numId').value.toUpperCase();
        this.requestUser.fechaIngreso = this.formUser.get('fechaIngreso').value.toUpperCase();
        this.requestUser.area = this.formUser.get('area').value.toUpperCase();
        this.requestUser.estado = this.formUser.get('estado').value.toUpperCase();
        this.requestUser.fechaRegistro = this.formUser.get('fechaRegistro').value;
        this.crearCorreo(this.requestUser);
        this.requestUser.correoElectronico = this.correo;

        //Consuminos el servicio
        this.services.createUser(this.requestUser).subscribe(data => {
            if (data.body) {
                alert('Usuario Registrado Exitosamente');
                this.requestUser = null;
            } else {
                alert('No se pudo registrar el Usuario.');
                this.requestUser = null;
            }
        });
        this.nav();
    }


    validarFechas() {

        let fechaI = moment(moment(this.formUser.get('fechaIngreso').value).format('YYYY-MM-DD'));
        let diasDiferencia = fechaI.diff(this.fechaMaxima, 'days');

        if (diasDiferencia > 0) {
            alert('Fecha no permitida');
            this.formUser.get('fechaIngreso').reset();

        }
    }

}
