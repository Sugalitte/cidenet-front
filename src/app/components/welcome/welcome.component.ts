import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {user} from '../../domain/user';
import {UsuariosServices} from '../../services/usuariosServices';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Persona} from '../../domain/Persona';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

    myTables: user[];
    tableData: any;
    formUser: FormGroup;
    estado: string;
    dataPersona: Persona;
    dataPersonaDelete: Persona;
    requestUser = new Persona();
    modal: NgbModalRef;

    constructor(private router: Router, private services: UsuariosServices, private modalService: NgbModal) {
    }

    ngOnInit() {
        this.initForm();
        this.dataTable();
        this.getUsers();
    }

    initForm() {
        this.formUser = new FormGroup({
                primerApellido: new FormControl(null, [Validators.required]),
                segundoApellido: new FormControl(null, [Validators.required]),
                primerNombre: new FormControl(null, [Validators.required]),
                segundoNombre: new FormControl(null, [Validators.required]),
                pais: new FormControl(null, [Validators.required]),
                tipoId: new FormControl(null, [Validators.required]),
                numId: new FormControl(null, [Validators.required]),
                correo: new FormControl(null, [Validators.required]),
                area: new FormControl(null, [Validators.required]),
                estado: new FormControl(null, [Validators.required]),
            }
        );
    }

    nav() {
        this.router.navigate(['registro']);
    }

    dataTable() {
        this.tableData = [
            {field: 'primerNombre', header: 'Primer Nombre'},
            {field: 'segundoNombre', header: 'Segundo Nombre'},
            {field: 'primerApellido', header: 'Primer Apellido'},
            {field: 'segundoApellido', header: 'Segundo Apellido'},
            {field: 'tipoIdentificacion', header: 'Tipo Identificación'},
            {field: 'numeroIdentificacion', header: 'Número de Identificación'},
            {field: 'pais', header: 'País'},
            {field: 'correoElectronico', header: 'Correo Electronico'},
            {field: 'fechaIngreso', header: 'Fecha Ingreso'},
            {field: 'area', header: 'Area'},
            {field: 'estado', header: 'Estado'},
            {field: 'fechaRegistro', header: 'Fecha Registro'}
        ];
        this.getUsers();
    }

    getUsers() {
        this.services.getUser().subscribe(data => {

            if (data.body == null) {
                alert('No hay usuarios Registrados');

            } else {
                this.myTables = data.body;
            }
        });
    }


    openDelete(del, dataUser: any) {
        this.modalService.open(del);
        this.requestDelte(dataUser);
    }

    requestDelte(dataUser: any) {
        this.dataPersonaDelete = new Persona();
        this.dataPersonaDelete.numeroIdentificacion = dataUser.persona.numeroIdentificacion;

    }

    deleteUser() {
        debugger
        this.services.deleteUser(this.dataPersonaDelete.numeroIdentificacion).subscribe(del => {
            if (del.status == 'OK') {
                alert('Usuario eliminado.');
                setTimeout(() => {
                    this.getUsers();
                }, 1000);
            } else {
                alert('Error eliminando Usuario.');
            }
        });
    }

    openEdit(edit, dataUser: any) {
        this.modalService.open(edit);
        this.requestEditUser(dataUser);
    }

    requestEditUser(dataUser: any) {
        this.dataPersona = new Persona();
        this.dataPersona.primerNombre = dataUser.persona.primerNombre;
        this.dataPersona.segundoNombre = dataUser.persona.segundoNombre;
        this.dataPersona.primerApellido = dataUser.persona.primerApellido;
        this.dataPersona.segundoApellido = dataUser.persona.segundoApellido;
        this.dataPersona.tipoIdentificacion = dataUser.persona.tipoIdentificacion;
        this.dataPersona.numeroIdentificacion = dataUser.persona.numeroIdentificacion;
        this.dataPersona.pais = dataUser.persona.pais;
        this.dataPersona.correoElectronico = dataUser.persona.correoElectronico;
        this.dataPersona.area = dataUser.persona.area;
        this.dataPersona.estado = dataUser.persona.estado;
    }

    editUser() {
        debugger
        this.requestUser.primerApellido = this.formUser.get('primerApellido').value.toUpperCase();
        this.requestUser.segundoApellido = this.formUser.get('segundoApellido').value.toUpperCase();
        this.requestUser.primerNombre = this.formUser.get('primerNombre').value.toUpperCase();
        this.requestUser.segundoNombre = this.formUser.get('segundoNombre').value.toUpperCase();
        this.requestUser.pais = this.formUser.get('pais').value.toUpperCase();
        this.requestUser.tipoIdentificacion = this.formUser.get('tipoId').value.toUpperCase();
        this.requestUser.numeroIdentificacion = this.formUser.get('numId').value.toUpperCase();
        this.requestUser.antiguoNumeroIdentificacion = this.dataPersona.numeroIdentificacion.toUpperCase();
        this.requestUser.area = this.formUser.get('area').value.toUpperCase();
        this.requestUser.estado = this.formUser.get('estado').value.toUpperCase();

        this.services.editUser(this.requestUser).subscribe(edit => {
            if (edit.status == 'OK') {
                alert('Usuario Editado Exitosamente');
                this.requestUser = null;
                this.getUsers();
            } else {
                alert('No se pudo editar el Usuario.');
                this.requestUser = null;
                this.getUsers();
            }
        });
    }
}
