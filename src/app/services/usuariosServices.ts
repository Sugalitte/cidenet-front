import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Persona} from '../domain/Persona';
import {ApiManagerService} from '../infraestructure/api-manager';

@Injectable({
    providedIn: 'root'
})
export class UsuariosServices {

    constructor(private http: HttpClient, private api: ApiManagerService) {
    }

    getUser(): Observable<any> {

        const endpoint = environment.getUsers;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        return this.http.get(endpoint, {headers: headers});
    }

    createUser(user: Persona): Observable<any> {
        const endpoint = environment.createUser;
        console.log(user);
        return this.api.post(endpoint, user);
    }

    editUser(user:Persona): Observable<any> {
        const endpoint = environment.editUser;
        console.log(user);
        return this.api.put(endpoint, user);
    }

    deleteUser(idUser:string): Observable<any> {
        const endpoint = environment.deleteUser+idUser;
        return this.api.delete(endpoint);
    }
}
