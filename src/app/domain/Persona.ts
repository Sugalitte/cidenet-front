export class Persona {

    primerNombre?: string;
    segundoNombre?: string;
    primerApellido?: string;
    segundoApellido?: string;
    tipoIdentificacion?: string;
    numeroIdentificacion?: string;
    antiguoNumeroIdentificacion?: string;
    pais?: string;
    correoElectronico?: string;
    fechaIngreso?: Date;
    area?: string;
    estado?: string;
    fechaRegistro?: Date;
    fechaModificacion?: Date;
}

