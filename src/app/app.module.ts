import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {RegistroComponent} from './components/registro/registro.component';
import {AppRoutingModule} from './app.routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApiManagerService} from './infraestructure/api-manager';
import {HttpHandlerService} from './infraestructure/util/httpHandler';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
    declarations: [
        AppComponent,
        WelcomeComponent,
        RegistroComponent
    ],
    imports: [
        BrowserModule,
        RouterModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,

    ],
    providers: [ApiManagerService, HttpHandlerService,
    ],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule {
}
