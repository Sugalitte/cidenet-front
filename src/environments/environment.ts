const apiCidenet = 'http://localhost:8090/cidenet/';

export const environment = {

  production: false,

  getUsers: `${apiCidenet}persona/getUser`,
  createUser: `${apiCidenet}persona/registro`,
  editUser: `${apiCidenet}persona/updUser`,
  deleteUser: `${apiCidenet}persona/deleteUser/`,

};
