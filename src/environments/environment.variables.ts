export const paises = {
    Colombia: 'COLOMBIA',
    USA: 'ESTADOS UNIDOS'
};

export const dominios = {
    Colombia: '@cidenet.com.co',
    USA: '@cidenet.com.us'
};
